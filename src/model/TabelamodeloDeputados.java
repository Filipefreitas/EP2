package model;

import javax.swing.table.AbstractTableModel;
import edu.unb.fga.dadosabertos.Deputado;
import model.DadosCamara;

public class TabelamodeloDeputados extends AbstractTableModel{

DadosCamara elementostabela = new DadosCamara();
///DadosDetalhes elementostabeladetalhes1 = new DadosDetalhes();

	public int getColumnCount() {
		return 10;
	}
	public int getRowCount() {
		return elementostabela.deputadolista.size();
	}
	public Object getValueAt(int rownindex, int columindex) {
		switch(columindex){
		case 0:
			return elementostabela.deputadolista.get(rownindex).getNomeParlamentar();
		case 1:
			return elementostabela.deputadolista.get(rownindex).getPartido();
		case 2:
			return elementostabela.deputadolista.get(rownindex).getSexo();
		case 3:
			return elementostabela.deputadolista.get(rownindex).getUf();
		case 4:
			return elementostabela.deputadolista.get(rownindex).getEmail();
		case 5:
			return elementostabela.deputadolista.get(rownindex).getFone();
		case 6:
			return elementostabela.deputadolista.get(rownindex).getGabinete();
		case 7:
			return elementostabela.deputadolista.get(rownindex).getIdeCadastro();
		case 8:
			return elementostabela.deputadolista.get(rownindex).getMatricula();
		case 9:
			return elementostabela.deputadolista.get(rownindex).getCondicao();
		case 10:
			return elementostabela.deputadolista.get(rownindex).getAnexo();
		}	
		return null;
	}
}