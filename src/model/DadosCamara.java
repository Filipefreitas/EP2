package model;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import javax.xml.bind.JAXBException;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;
import edu.unb.fga.dadosabertos.Detalhes;
import edu.unb.fga.dadosabertos.Partido;

public class DadosCamara {
public  List<Deputado> deputadolista;

public DadosCamara(){
		
	final Camara camara = new Camara();
		
		try {
			camara.obterDados();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
			
	
		
		deputadolista = camara.getDeputados();
}

public List<Deputado> getDeputadolista() {
	return deputadolista;
}
}

