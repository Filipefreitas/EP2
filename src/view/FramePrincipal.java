package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.ScrollPane;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollBar;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import java.awt.SystemColor;
import javax.swing.JList;
import javax.swing.table.DefaultTableModel;

import model.TabelamodeloDeputados;
import javax.swing.JComboBox;
import java.awt.Window.Type;
import java.awt.Dimension;

public class FramePrincipal extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTable table;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FramePrincipal frame = new FramePrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("rawtypes")
	public FramePrincipal() {
		setResizable(false);
		setType(Type.UTILITY);
		setBackground(UIManager.getColor("Tree.selectionBorderColor"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 500);
		contentPane = new JPanel();
		contentPane.setForeground(Color.YELLOW);
		contentPane.setBackground(new Color(51, 153, 153));
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(new LineBorder(new Color(51, 153, 153), 1, true));
		tabbedPane.setSize(new Dimension(1026, 500));
		tabbedPane.setBackground(Color.WHITE);
		contentPane.add(tabbedPane);
		
		JPanel abaDeputados = new JPanel();
		abaDeputados.setBackground(Color.WHITE);
		abaDeputados.setBorder(new EmptyBorder(0, 0, 0, 0));
		tabbedPane.addTab("Deputados", null, abaDeputados, null);
		abaDeputados.setLayout(null);
		
		JButton pesquisar = new JButton("");
		pesquisar.setBackground(SystemColor.controlLtHighlight);
		pesquisar.setIcon(new ImageIcon(FramePrincipal.class.getResource("/imagens/lupa12.png")));
		pesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		pesquisar.setBounds(595, 0, 36, 36);
		abaDeputados.add(pesquisar);
		
		textField = new JTextField();
		textField.setBounds(237, 5, 357, 24);
		abaDeputados.add(textField);
		textField.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(2, 41, 1018, 397);
		abaDeputados.add(scrollPane);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(41, 5, 171, 24);
		abaDeputados.add(comboBox);
		
		TabelamodeloDeputados modelodeputado = new TabelamodeloDeputados();
		table = new JTable();
		table.setCellSelectionEnabled(true);
		table.setModel(modelodeputado);
		table.setBounds(12, 48, 619, 390);
		scrollPane.setViewportView(table);
		
		
	
		JPanel abaPartido = new JPanel();
		abaPartido.setBackground(Color.WHITE);
		abaPartido.setBorder(new EmptyBorder(0, 0, 0, 0));
		tabbedPane.addTab("Partido", null, abaPartido, null);
		
		JButton pesquisa = new JButton();
		pesquisa.setBounds(595, 0, 36, 36);
		pesquisa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		abaPartido.setLayout(null);
		pesquisa.setBackground(Color.WHITE);
		pesquisa.setIcon(new ImageIcon(FramePrincipal.class.getResource("/imagens/lupa12.png")));
		abaPartido.add(pesquisa);
		
		textField_1 = new JTextField();
		textField_1.setBounds(237, 5, 357, 24);
		abaPartido.add(textField_1);
		textField_1.setColumns(10);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(2, 40, 1018, 397);
		abaPartido.add(scrollPane_1);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(41, 5, 171, 24);
		abaPartido.add(comboBox_1);
		
		table_1 = new JTable();
		table_1.setCellSelectionEnabled(true);
		table_1.setModel(modelodeputado);
		table_1.setBounds(12, 48, 619, 390);
		///scrollPane_1.setViewportView(table_1);
			}
}